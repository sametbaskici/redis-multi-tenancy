package com.example.redis.multitenancy.storage;

import com.example.redis.multitenancy.config.model.TenancyConfig;
import com.example.redis.multitenancy.config.model.TenancyConfigModel;
import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Profile("multiRedisTemplate")
@Getter
@Component
public class RedisConnectionPool {

    private static final String JEDIS_CONNECTION_FACTORY_BEAN_NAME_PREFIX = "jedisConnectionFactory-";
    private static final String REDIS_TEMPLATE_BEAN_NAME_PREFIX = "redisTemplate-";

    private ApplicationContext applicationContext;

    @Getter(value = AccessLevel.PRIVATE)
    private TenancyConfig tenancyConfig;

    private Map<String, RedisTemplate> redisTemplateMap;

    @Autowired
    public RedisConnectionPool(ApplicationContext applicationContext, TenancyConfig tenancyConfig) {
        this.applicationContext = applicationContext;
        this.tenancyConfig = tenancyConfig;
    }

    @PostConstruct
    private void init() {
        initFactoryBean(tenancyConfig.getTenancies());
        initRedisTemplates();
        //testBeanNames();
        //initRedisTemplateMap();
    }

    public RedisTemplate getRedisTemplateByTenancyId(String tenantId) {
        return getRedisTemplateMap().get(REDIS_TEMPLATE_BEAN_NAME_PREFIX + tenantId);
    }

    private void initFactoryBean(List<TenancyConfigModel> tenancyConfigModels) {
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.rootBeanDefinition(JedisConnectionFactory.class);
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();

        for (TenancyConfigModel tenancy : tenancyConfig.getTenancies()) {
            builder.addPropertyValue("hostName", tenancy.getRedisHost());
            builder.addPropertyValue("portName", tenancy.getRedisPort());
            factory.registerBeanDefinition(JEDIS_CONNECTION_FACTORY_BEAN_NAME_PREFIX + tenancy.getId(), builder.getBeanDefinition());
        }
    }

    private void initRedisTemplates() {
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.rootBeanDefinition(RedisTemplate.class);
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();

        for (TenancyConfigModel tenancy : tenancyConfig.getTenancies()) {
            builder.addPropertyReference("connectionFactory", JEDIS_CONNECTION_FACTORY_BEAN_NAME_PREFIX + tenancy.getId());
            factory.registerBeanDefinition(REDIS_TEMPLATE_BEAN_NAME_PREFIX + tenancy.getId(), builder.getBeanDefinition());
        }
    }

    private void initRedisTemplateMap() {
        for (TenancyConfigModel tenancy : tenancyConfig.getTenancies()) {
            RedisTemplate<String, ?> redisTemplate = (RedisTemplate<String, ?>) applicationContext.getBean(REDIS_TEMPLATE_BEAN_NAME_PREFIX + tenancy.getId());
            this.redisTemplateMap.put(tenancy.getId(), redisTemplate);
        }
    }

    //private void testBeanNames(){
//
    //    DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
//
    //    RedisTemplate<String, ? > redisTemplate = (RedisTemplate<String, ?>) beanFactory.getBean(REDIS_TEMPLATE_BEAN_NAME_PREFIX+"tenant_1");
    //    System.out.println("___");
//
    //    //String[] beans = applicationContext.getBeanDefinitionNames();
    //    //for(String bean: beans){
    //    //    System.out.println("bean : " + bean);
    //    //}
    //}

}
