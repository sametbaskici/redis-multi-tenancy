package com.example.redis.multitenancy.storage;

import com.example.redis.multitenancy.config.model.TenancyConfig;
import com.example.redis.multitenancy.config.model.TenancyConfigModel;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Profile("multiRedisTemplate")
@Service
public class RedisTemplateService {

    private static final String JEDIS_CONNECTION_FACTORY_BEAN_NAME_PREFIX = "jedisConnectionFactory_";
    private static final String REDIS_TEMPLATE_BEAN_NAME_PREFIX = "redisTemplate_";

    private TenancyConfig tenancyConfig;
    private ApplicationContext applicationContext;

    public RedisTemplateService(TenancyConfig tenancyConfig, ApplicationContext applicationContext) {
        this.tenancyConfig = tenancyConfig;
        this.applicationContext = applicationContext;
    }

    private Map<String, RedisTemplate> redisTemplateMap;

    @PostConstruct
    private void init(){
        initRedisTemplateMap();

    }

    public RedisTemplate getByTenantId(String tenantId){
        return redisTemplateMap.get(tenantId);
    }

    private void initRedisTemplateMap(){
        redisTemplateMap = new HashMap<>();

        for(TenancyConfigModel tenancy: tenancyConfig.getTenancies()){
            RedisTemplate<String,?> redisTemplate = (RedisTemplate<String, ?>) applicationContext.getBean(REDIS_TEMPLATE_BEAN_NAME_PREFIX + tenancy.getId());
            redisTemplateMap.put(tenancy.getId(),redisTemplate);
        }
    }

}
