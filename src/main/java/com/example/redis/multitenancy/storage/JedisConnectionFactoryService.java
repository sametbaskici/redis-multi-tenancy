package com.example.redis.multitenancy.storage;

import com.example.redis.multitenancy.config.model.TenancyConfig;
import com.example.redis.multitenancy.config.model.TenancyConfigModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Profile("singleRedisTemplate")
@Service
public class JedisConnectionFactoryService {

    private static final String JEDIS_CONNECTION_FACTORY_BEAN_NAME_PREFIX = "jedisConnectionFactory_";

    private RedisTemplate redisTemplate;
    private TenancyConfig tenancyConfig;
    private ApplicationContext applicationContext;
    private JedisConnectionFactory defaultJedisConnectionFactory;

    @Autowired
    private Map<String, JedisConnectionFactory> jedisConnectionFactoryMap;

    public JedisConnectionFactoryService(RedisTemplate redisTemplate, TenancyConfig tenancyConfig, ApplicationContext applicationContext, JedisConnectionFactory jedisConnectionFactory) {
        this.redisTemplate = redisTemplate;
        this.tenancyConfig = tenancyConfig;
        this.applicationContext = applicationContext;
        this.defaultJedisConnectionFactory = jedisConnectionFactory;
    }

    public void switchTenant(String tenantId){
        if(StringUtils.isEmpty(tenantId)){
            redisTemplate.setConnectionFactory(defaultJedisConnectionFactory);
        }else{
            redisTemplate.setConnectionFactory(jedisConnectionFactoryMap.get(tenantId));
        }

    }

    @PostConstruct
    private void init(){
        initJedisConnectionFactoryMap();
    }

    private void initJedisConnectionFactoryMap(){
        jedisConnectionFactoryMap = new HashMap<>();
        for(TenancyConfigModel tenancy: tenancyConfig.getTenancies()){
            JedisConnectionFactory jedisConnectionFactory = (JedisConnectionFactory) applicationContext.getBean(JEDIS_CONNECTION_FACTORY_BEAN_NAME_PREFIX + tenancy.getId());
            jedisConnectionFactoryMap.put(tenancy.getId(),jedisConnectionFactory);
        }
    }


}
