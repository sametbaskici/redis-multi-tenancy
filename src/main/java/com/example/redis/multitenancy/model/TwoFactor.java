package com.example.redis.multitenancy.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@Builder
@Data
@RedisHash("twoFactor")
public class TwoFactor {

    @Id
    @Indexed
    private String guid;
    private String name;
    private String email;

}
