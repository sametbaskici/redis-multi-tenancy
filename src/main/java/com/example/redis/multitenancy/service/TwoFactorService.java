package com.example.redis.multitenancy.service;

import com.example.redis.multitenancy.model.Tenantable;
import com.example.redis.multitenancy.model.TwoFactor;
import com.example.redis.multitenancy.repository.TwoFactorRedisRepository;
import org.springframework.stereotype.Service;


@Service
public class TwoFactorService {

    private final TwoFactorRedisRepository twoFactorRedisRepository;

    public TwoFactorService(TwoFactorRedisRepository twoFactorRedisRepository) {
        this.twoFactorRedisRepository = twoFactorRedisRepository;
    }

    public TwoFactor find(String id) {
        return twoFactorRedisRepository.findOne(id);
    }

    @Tenantable
    public void save(TwoFactor twoFactor) {
        twoFactorRedisRepository.save(twoFactor);
    }


}
