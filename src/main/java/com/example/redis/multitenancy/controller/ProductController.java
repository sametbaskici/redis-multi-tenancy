package com.example.redis.multitenancy.controller;

import com.example.redis.multitenancy.model.Product;
import com.example.redis.multitenancy.repository.ProductRepository;
import com.example.redis.multitenancy.service.ProductService;
import com.example.redis.multitenancy.storage.JedisConnectionFactoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/product/add/{name}")
    public void saveProduct(@PathVariable String name){
        Product product = Product.builder()
                .brand("x brand")
                .model("y model")
                .name(name)
                .build();

        productService.save(product);


    }
}
