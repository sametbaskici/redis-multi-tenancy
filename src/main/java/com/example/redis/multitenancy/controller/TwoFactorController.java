package com.example.redis.multitenancy.controller;

import com.example.redis.multitenancy.model.TwoFactor;
import com.example.redis.multitenancy.service.TwoFactorService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TwoFactorController {

    private final TwoFactorService twoFactorService;

    public TwoFactorController(TwoFactorService twoFactorService) {
        this.twoFactorService = twoFactorService;
    }

    @GetMapping("/twofactor/add/{guid}")
    public void add(@PathVariable String guid){

        TwoFactor twoFactor = TwoFactor.builder()
                .email("sss@qqq.com")
                .guid(guid)
                .name("name")
                .build();
        twoFactorService.save(twoFactor);

    }

    @GetMapping("/twofactor/get/{guid}")
    public TwoFactor get(String guid){
        return twoFactorService.find(guid);
    }
}
