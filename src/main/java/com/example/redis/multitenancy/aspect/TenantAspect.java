package com.example.redis.multitenancy.aspect;

import com.example.redis.multitenancy.config.TenantContext;
import com.example.redis.multitenancy.storage.JedisConnectionFactoryService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@Aspect
public class TenantAspect {

    @Autowired
    private JedisConnectionFactoryService jedisConnectionFactoryService;

    @Pointcut("execution(public * *(..))")
    public void publicMethod() {}

    //@Pointcut("! this(org.springframework.data.jpa.repository.JpaRepository+)")
    @Pointcut("execution(public * org.springframework.data.jpa.repository.JpaRepository+.*(..))")
    public void jpaRepository(){}

    @Pointcut("@annotation(com.example.redis.multitenancy.model.Tenantable)")
    public void tenantableOnMethod(){}

    @Pointcut("@within(com.example.redis.multitenancy.model.Tenantable)")
    public void tenantableOnClass(){}

    @Pointcut("tenantableOnMethod() || tenantableOnClass()")
    public void tenantable(){}

    @Pointcut("execution(public * org.springframework.data.repository.Repository+.*(..))")
    public void repository(){}

    @Around("tenantable()")
    public Object repositoryExec(ProceedingJoinPoint joinPoint) throws Throwable {
        log.info("switching tenant to  : " + TenantContext.getTenantId());
        jedisConnectionFactoryService.switchTenant(TenantContext.getTenantId());
        return joinPoint.proceed();
    }

}
