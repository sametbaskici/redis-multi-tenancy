package com.example.redis.multitenancy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisMultitenancyApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisMultitenancyApplication.class, args);
    }
}
