package com.example.redis.multitenancy.repository;

import com.example.redis.multitenancy.model.Tenantable;
import com.example.redis.multitenancy.model.TwoFactor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Tenantable
@Repository
public interface TwoFactorRedisRepository extends CrudRepository<TwoFactor, String> {
}
