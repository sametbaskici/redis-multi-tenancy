package com.example.redis.multitenancy.config.model;

import lombok.Data;

@Data
public class TenancyConfigModel {
    private String name;
    private Integer redisPort;
    private String redisHost;
    private String id;
}
