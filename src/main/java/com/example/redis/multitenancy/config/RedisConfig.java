package com.example.redis.multitenancy.config;

import com.example.redis.multitenancy.config.model.TenancyConfig;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

@Data
@Configuration
public class RedisConfig {

    private static final String JEDIS_CONNECTION_FACTORY_BEAN_NAME_PREFIX = "jedisConnectionFactory_";
    private static final String REDIS_TEMPLATE_BEAN_NAME_PREFIX = "redisTemplate_";

    @Autowired
    private TenancyConfig tenancyConfig;

    @Primary
    @Bean("connectionFactory")
    public RedisConnectionFactory connectionFactory() {
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName("localhost");
        jedisConnectionFactory.setPort(6379);
        jedisConnectionFactory.afterPropertiesSet();
        return jedisConnectionFactory;
    }


    //@Bean(name = "jedisConnectionFactories" )
    //public Map<String, JedisConnectionFactory> JedisConnectionFactor(){
    //    Map<String, JedisConnectionFactory> jedisConnectionFactoryMap = new HashMap<>();
    //    for(TenancyConfigModel tenancy : tenancyConfig.getTenancies()){
    //        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
    //        jedisConnectionFactory.setHostName(tenancy.getRedisHost());
    //        jedisConnectionFactory.setPort(tenancy.getRedisPort());
    //        jedisConnectionFactoryMap.put(tenancy.getId(),jedisConnectionFactory);
    //    }
    //    return jedisConnectionFactoryMap;
    //}


   @Bean("jedisConnectionFactory_tenant_1")
   public RedisConnectionFactory connectionFactoryTenancy1() {
       JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
       jedisConnectionFactory.setHostName("127.0.0.1");
       jedisConnectionFactory.setPort(8091);
       jedisConnectionFactory.afterPropertiesSet();
       return jedisConnectionFactory;
   }

   @Bean("jedisConnectionFactory_tenant_2")
   public RedisConnectionFactory connectionFactoryTenancy2() {
       JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
       jedisConnectionFactory.setHostName("127.0.0.1");
       jedisConnectionFactory.setPort(8092);
       jedisConnectionFactory.afterPropertiesSet();
       return jedisConnectionFactory;
   }

   @Bean("jedisConnectionFactory_tenant_3")
   public RedisConnectionFactory connectionFactoryTenancy3() {
       JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
       jedisConnectionFactory.setHostName("127.0.0.1");
       jedisConnectionFactory.setPort(8093);
       jedisConnectionFactory.afterPropertiesSet();
       return jedisConnectionFactory;
   }

   @Bean("jedisConnectionFactory_tenant_4")
   public RedisConnectionFactory connectionFactoryTenancy4() {
       JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
       jedisConnectionFactory.setHostName("127.0.0.1");
       jedisConnectionFactory.setPort(8094);
       jedisConnectionFactory.afterPropertiesSet();
       return jedisConnectionFactory;
   }

   @Bean("jedisConnectionFactory_tenant_5")
   public RedisConnectionFactory connectionFactoryTenancy5() {
       JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
       jedisConnectionFactory.setHostName("127.0.0.1");
       jedisConnectionFactory.setPort(8095);
       jedisConnectionFactory.afterPropertiesSet();
       return jedisConnectionFactory;
   }

   /* @Bean("jedisConnectionFactory_tenant_6")
    public RedisConnectionFactory connectionFactoryTenancy6() {
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName("127.0.0.1");
        jedisConnectionFactory.setPort(8096);
        jedisConnectionFactory.afterPropertiesSet();
        return jedisConnectionFactory;
    }

    @Bean("jedisConnectionFactory_tenant_7")
    public RedisConnectionFactory connectionFactoryTenancy7() {
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName("127.0.0.1");
        jedisConnectionFactory.setPort(8097);
        jedisConnectionFactory.afterPropertiesSet();
        return jedisConnectionFactory;
    }

    @Bean("jedisConnectionFactory_tenant_8")
    public RedisConnectionFactory connectionFactoryTenancy8() {
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName("127.0.0.1");
        jedisConnectionFactory.setPort(8098);
        jedisConnectionFactory.afterPropertiesSet();
        return jedisConnectionFactory;
    }

    @Bean("jedisConnectionFactory_tenant_9")
    public RedisConnectionFactory connectionFactoryTenancy9() {
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName("127.0.0.1");
        jedisConnectionFactory.setPort(8099);
        jedisConnectionFactory.afterPropertiesSet();
        return jedisConnectionFactory;
    }*/




    //@Bean("redisTemplate_tenant_1")
    //public RedisTemplate<String, ?> redisTemplateTenancy1() {
    //    RedisTemplate<String, ?> redisTemplate = new RedisTemplate<>();
    //    redisTemplate.setConnectionFactory(connectionFactoryTenancy1());
    //    redisTemplate.setKeySerializer(new StringRedisSerializer());
    //    redisTemplate.afterPropertiesSet();
    //    return redisTemplate;
    //}

   //@Bean("redisTemplate_tenant_2")
   //public RedisTemplate<String, ?> redisTemplateTenancy2() {
   //    RedisTemplate<String, ?> redisTemplate = new RedisTemplate<>();
   //    redisTemplate.setConnectionFactory(connectionFactoryTenancy2());
   //    redisTemplate.setKeySerializer(new StringRedisSerializer());
   //    redisTemplate.afterPropertiesSet();
   //    return redisTemplate;
   //}

   //@Bean("redisTemplate_tenant_3")
   //public RedisTemplate<String, ?> redisTemplateTenancy3() {
   //    RedisTemplate<String, ?> redisTemplate = new RedisTemplate<>();
   //    redisTemplate.setConnectionFactory(connectionFactoryTenancy3());
   //    redisTemplate.setKeySerializer(new StringRedisSerializer());
   //    redisTemplate.afterPropertiesSet();
   //    return redisTemplate;
   //}

   //@Bean("redisTemplate_tenant_4")
   //public RedisTemplate<String, ?> redisTemplateTenancy4() {
   //    RedisTemplate<String, ?> redisTemplate = new RedisTemplate<>();
   //    redisTemplate.setConnectionFactory(connectionFactoryTenancy4());
   //    redisTemplate.setKeySerializer(new StringRedisSerializer());
   //    redisTemplate.afterPropertiesSet();
   //    return redisTemplate;
   //}

   //@Bean("redisTemplate_tenant_5")
   //public RedisTemplate<String, ?> redisTemplateTenancy5() {
   //    RedisTemplate<String, ?> redisTemplate = new RedisTemplate<>();
   //    redisTemplate.setConnectionFactory(connectionFactoryTenancy5());
   //    redisTemplate.setKeySerializer(new StringRedisSerializer());
   //    redisTemplate.afterPropertiesSet();
   //    return redisTemplate;
   //}

}
