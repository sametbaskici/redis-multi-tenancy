package com.example.redis.multitenancy.config.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties("redis")
public class TenancyConfig {
    private List<TenancyConfigModel> tenancies;

    public TenancyConfigModel getByTenancyId(String id) {
        return getTenancies().stream()
                .filter(tenancy -> tenancy.getId().equals(id))
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }
}
